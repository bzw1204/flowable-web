import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
  {
    path: '/',
    component: Layout,
    redirect: '/modules',
    children: [
      {
        path: 'modules',
        name: 'Template',
        component: () => import('@/views/engine/TemplateList'),
        meta: { title: '模板管理', icon: 'table' }
      },
      {
        path: 'bpmn',
        name: 'BpmnModeler',
        hidden: true,
        component: () => import('@/views/bpmn/index'),
        meta: { title: '画图', icon: 'tree' }
      }
    ]
  },
  {
    path: '/',
    component: Layout,
    redirect: '/definition',
    children: [{
      path: 'definition',
      name: 'Definition',
      component: () => import('@/views/engine/DefinitionList'),
      meta: { title: '定义管理', icon: 'tree' }
    }]
  },
  // {
  //   path: '/',
  //   component: Layout,
  //   redirect: '/instance',
  //   children: [{
  //     path: 'instance',
  //     name: 'Instance',
  //     component: () => import('@/views/engine/InstanceList'),
  //     meta: { title: '实例管理', icon: 'tree' }
  //   }]
  // },
  {
    path: '/',
    component: Layout,
    redirect: '/task',
    children: [{
      path: 'task',
      name: 'Task',
      component: () => import('@/views/task/TaskList'),
      meta: { title: '任务管理', icon: 'tree' }
    }]
  },
  {
    path: '/example',
    component: Layout,
    hidden: true,
    redirect: '/example/table',
    name: 'Example',
    meta: { title: '引擎管理', icon: 'example' },
    children: [
      {
        path: 'modules',
        name: 'Template',
        component: () => import('@/views/engine/TemplateList'),
        meta: { title: '模板管理', icon: 'table' }
      },
      {
        path: 'definition',
        name: 'Definition',
        component: () => import('@/views/engine/DefinitionList'),
        meta: { title: '定义管理', icon: 'tree' }
      },
      // {
      //   path: 'instance',
      //   name: 'Instance',
      //   component: () => import('@/views/engine/InstanceList'),
      //   meta: { title: '实例管理', icon: 'tree' }
      // },
      {
        path: 'task',
        name: 'Task',
        component: () => import('@/views/task/TaskList'),
        meta: { title: '任务管理', icon: 'tree' }
      },
      {
        path: 'bpmn',
        name: 'BpmnModeler',
        hidden: true,
        component: () => import('@/views/bpmn/index'),
        meta: { title: '画图', icon: 'tree' }
      }
    ]
  },

  {
    path: '/form',
    component: Layout,
    name: 'Form',
    hidden: true,
    meta: { title: '测试案例', icon: 'form' },
    children: [
      {
        path: 'cases',
        name: 'Form',
        component: () => import('@/views/form/index'),
        meta: { title: '案件申请', icon: 'form' }
      },
      {
        path: 'leave',
        name: 'Form',
        component: () => import('@/views/form/index'),
        meta: { title: '请假申请', icon: 'form' }
      }
    ]
  },
  {
    path: 'external-link',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
        meta: { title: 'External Link', icon: 'link' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
