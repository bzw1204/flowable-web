import request from '@/utils/request'

export function templateXml(parameter) {
  return request({
    url: `/server/rest/model/loadXmlByModelId/${parameter}`,
    method: 'get'
  })
}
export function addModel(parameter) {
  return request({
    url: '/server/rest/model/addModel',
    method: 'post',
    data: parameter
  })
}
