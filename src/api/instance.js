import request from '@/utils/request'
import Qs from 'qs'
export function instanceList(parameter) {
  return request({
    url: '/server/rest/processInstance/page-model',
    method: 'post',
    data: parameter
  })
}

export function finishFun(parameter) {
  const data = Qs.stringify(parameter)
  return request({
    url: '/server/rest/processInstance/stopProcess',
    method: 'post',
    data: data
  })
}

export function hangOrActive(parameter) {
  const data = Qs.stringify(parameter)
  return request({
    url: '/server/rest/processInstance/saProcessInstanceById',
    method: 'post',
    data: data
  })
}
export function deleteProcessInstanceById(parameter) {
  return request({
    url: `/server/rest/processInstance/deleteProcessInstanceById/${parameter.processInstanceId}`,
    method: 'get'
  })
}

