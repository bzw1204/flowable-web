import request from '@/utils/request'

export function getModuleList(params) {
  return request({
    url: '/server/rest/model/page-model',
    method: 'get',
    params
  })
}

export function deploy(modelId) {
  return request({
    url: `/server/rest/model/deploy?modelId=${modelId}`,
    method: 'post'
  })
}
