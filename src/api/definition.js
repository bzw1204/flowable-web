import request from '@/utils/request'
import Qs from 'qs'
export function instanceList(parameter) {
  const data = Qs.stringify(parameter)
  return request({
    url: '/server/rest/definition/page-model',
    method: 'post',
    data: data
  })
}
export function hangOrActive(parameter) {
  const data = Qs.stringify(parameter)
  return request({
    url: '/server/rest/definition/saDefinitionById',
    method: 'post',
    data: data
  })
}
export function deleteDeployment(parameter) {
  const data = Qs.stringify({ 'deploymentId': parameter.deploymentId })
  return request({
    url: `/server/rest/definition/deleteDeployment`,
    method: 'post',
    data: data
  })
}
//
