import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/server/rest/model/page-model',
    method: 'get',
    params
  })
}
