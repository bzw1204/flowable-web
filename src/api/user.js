import request from '@/utils/request'
import Qs from 'qs'

export function login(parameter) {
  const data = Qs.stringify(parameter)
  return request({
    url: '/server/app/authentication',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/server/app/rest/account',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/server/app/logout',
    method: 'post'
  })
}
