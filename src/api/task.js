import request from '@/utils/request'
import Qs from 'qs'

export function upcomingList(parameter) {
  return request({
    url: '/server/rest/task/get-applying-tasks',
    method: 'get',
    params: parameter
  })
}

export function processedList(parameter) {
  return request({
    url: '/server/rest/task/get-applyed-tasks',
    method: 'get',
    params: parameter
  })
}

export function processInstances(parameter) {
  return request({
    url: '/server/rest/task/my-processInstances',
    method: 'get',
    params: parameter
  })
}

export function approvalComments(parameter) {
  return request({
    url: `/server/rest/formdetail/commentsByProcessInstanceId?processInstanceId=${parameter}`,
    method: 'get'
  })
}

export function createCases(parameter) {
  const data = Qs.stringify(parameter)
  return request({
    url: '/server/rest/leave/add',
    method: 'post',
    data: data
  })
}
export function dealCase(parameter) {
  const data = Qs.stringify(parameter)
  return request({
    url: '/server/rest/formdetail/complete',
    method: 'post',
    data: data
  })
}
export function findAdd(parameter) {
  const data = Qs.stringify(parameter)
  return request({
    url: '/server/rest/leave/findAdd',
    method: 'post',
    data: data
  })
}

export function formInfo(parameter) {
  const data = Qs.stringify(parameter)
  return request({
    url: '/server/rest/task/find-formInfo',
    method: 'post',
    data: data
  })
}

export function assignGroup(parameter) {
  return request({
    url: '/server/rest/leave/getGroupList',
    method: 'get'
  })
}

export function assignUser(parameter) {
  return request({
    url: '/server/rest/leave/getUserList',
    method: 'get'
  })
}

export function assignTo(parameter) {
  return request({
    url: '/server/rest/formdetail/distributionTask',
    method: 'post',
    data: parameter
  })
}
